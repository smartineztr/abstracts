package com.jmendozadi;

public class Square extends Geometric {


    public Square(double side) {

        super("Square", 4, side);
    }

    @Override
    public void areaCalculation()
    {
        area=this.side*this.side;
    }
    @Override
    public void perimeterCalculation()
    {
        perimeter=this.side*4;
    }

    @Override
    public void radiusCalculation() {

    }


}
